# Populationbot
A wikipedia bot which fetches the cities/towns 2011 population from http://censusindia.gov.in and update it to wikipedia town/city pages. 
# Requirements
Requires pywikibit and Requests

use ```git clone --recursive https://gerrit.wikimedia.org/r/pywikibot/core.git``` to clone

use ``` pip install requests``` to install Requsts package
