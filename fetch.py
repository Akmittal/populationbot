from urllib.request import urlopen,Request
import sqlite3
import requests
import urllib
import json
import sqlite3, re
from bs4 import BeautifulSoup
import pywikibot

#connect to db
db = sqlite3.connect("data.db")
c = db.cursor()
# c.execute('''drop table urls''')
# c.execute('''drop table cities''')
c.execute('''create table if not exists urls 
	(name text,state text,url text)''')
c.execute('''create table if not exists cities 
	(name text,state text,wikiurls text, population integer)''')
db.commit()

def get_population(url):
	real_url="http://www.censusindia.gov.in/pca/"+url
	pop_page= requests.get(real_url)
	soup = BeautifulSoup(pop_page.text)
	pop_tab = soup.find_all('table',id='gvPopulation')
	return pop_tab[0].find_all("tr")[1].find_all("td")[1].string


def get_url(city,state,r):
	data={
		"TextBox1":city,
		"Button1":"Search",
		"q":city,
		"__VIEWSTATE":"dlKieyLE8oOYJ+ioU0frGB/6r9MWhpX3NpbxpJ0nVdl+JdqGhJSfM0vo0sW+0adEFIlFPbrBsq/Reb61w9tI2D319cntlbF3gxZefXMUTinAykK6PBwLSPQ+OgjppT37KS0eC1SwUMaYTfKyArHe9xeP9gk+2dleCSAW/hcusY4KVsQlqxTFmjQ+YWVfAn1AX2UWZo/RwMQ901QU+yq9lwqXySziqvY1WttJeMw4yTL/W1GIxxYiK+sAbjN34v94C/6ykH6Jiq7Dlff0NeMgdLiP26qdswVM/ZKvF5BPLSkKiUIlZv7dWT4cvmMP90egjF51nQ==",
		"__VIEWSTATEGENERATOR":"A9EAC34E",
		"__EVENTVALIDATION":"zGCfVqP+zMcxpmNg5+eBtSfpZAJ+tY4Tyv+nMPq/FRN97XXrhepyzKLAk/PRqMs7gnpyeNu/WGAK/ICO++8F+Y+ty4Xg0C4PWZtZatRbhja79/5A",
		"__VIEWSTATEENCRYPTED":""
	}
	headers={
		"Content-Type":"application/x-www-form-urlencoded",
		"User-Agent":"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36",
		"content-length":2554
	}
	cookies = {'sdmenu_my_menu':100000,'ASP.NET_SessionId':'ca4crhnvvjdiuk551qvjwovg'}
	re = requests.post("http://www.censusindia.gov.in/pca/Searchdata.aspx", data=data,headers=headers,cookies=r.cookies)
	cities_page=BeautifulSoup(re.text)
	list = cities_page.find_all("table",id="grdtownvillage")
	clist=[]
	for i in list:
		td_list=i.find_all("tr")
		for j in td_list:
			for k in j.find_all("td"):
				if k!=None:
					clist.append(k)
	count = 0
	for i in range(0,len(clist),6):
		state_name = clist[i].string[:-4].strip()
		town_name= clist[i+5].a.string.strip()
		url = clist[i+5].a['href'].strip()
		bracket_pos = town_name.find("(")

		if(bracket_pos!=-1):
			town_name=town_name[0:bracket_pos].strip()
	
		if town_name==city and state_name==state:
			ret_url=url
			count+=1
	if count == 1:
		return ret_url
		#insert into db
		#c.execute('''insert into urls values(?,?,?)''',(town_name,state_name,url))

#replace population in wikipage
def replace_pop(url,count):
        site = pywikibot.Site('en', 'wikipedia')  # The site we want to run our bot on
        page = pywikibot.Page(site, url)
        data = page.text
        m = re.sub(r"population_total[\s]*=[\s]*\d[\d]*(,\d+)*","population_total      ="+count ,data)
        if m:
        	page.text = m
        	page.save("population updated")

#fetch cities from wiki town list
soup = BeautifulSoup(open("index.html",encoding='utf-8'))
list=soup.find_all("table")
ws=['wikitable', 'sortable','jquery-tablesorter']
cities=[]
wikiurls = []
for i in list:
	if i['class']==ws:
		lst =i.find_all("td")
		
		for l in lst:
			if l.a!=None:
				cities.append(l.a.string)
				wikiurls.append(l.a['title'])

for i in range(0,len(cities),2):
	
	c.execute('insert into cities (name,state,wikiurls) values (?,?,?)',(cities[i],cities[i+1],wikiurls[i]))
db.commit()

#get request cookies
r = requests.get("http://www.censusindia.gov.in/pca/Searchdata.aspx")
	#print(row)
for row in c.execute('select * from cities'):
	log=open("log.txt","a+")
	town_url = get_url(row[0],row[1],r)
	if town_url!=None:
		getcount = get_population(town_url)
		if getcount:
			replace_pop(row[2],getcount)
			log.write("data updated for "+row[0]+" "+row[1])

db.commit()
log.close()
#fetch cities and corresponding population fron censusindia site
db.close()
	
