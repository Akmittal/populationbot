/*  _____________________________________________________________________________
 * |                                                                             |
 * |                    === WARNING: GLOBAL GADGET FILE ===                      |
 * |                  Changes to this page affect many users.                    |
 * | Please discuss changes on the talk page or on [[WT:Gadget]] before editing. |
 * |_____________________________________________________________________________|
 *
 * Imported from version 1.0 as of 17:18, 9 April 2011 (UTC) from [[User:Smith609/citations.js]]
 * Adds a "Citation bot" link to the toolbox, and a button beneath edit fields, see [[User:Smith609/citations]]
 */

// @Version 1.0 - Adds an "Expand citations" link to the toolbox, and a button beneath edit fields.

function addCBToToolbox() {
  if ( mw.config.get( 'wgCanonicalNamespace' ) !== 'Special' ) {
   var pTb = document.getElementById("p-tb");
   if( !pTb ) return;
   var escPageName = encodeURIComponent( mw.config.get( 'wgPageName' ));

   mw.util.addPortletLink("p-tb", "//tools.wmflabs.org/citations/doibot.php?edit=toolbar&slow=1&user="+encodeURIComponent( mw.config.get( 'wgUserName' ))+"&page="+escPageName, 'Expand citations', '', "Expand citations and fix common formatting errors");
    }
}

// Add a citation bot button
function addCitationBot() {
	if (document.getElementById('wpDiff')){
	 var diff = document.getElementById('wpDiff');
	 var cbButton = document.createElement("input");
	 cbButton.value="Citations";
	 cbButton.type="submit";
	 originalAction=document.getElementById('editform').getAttribute('action');
	 cbButton.setAttribute("onmousedown", " document.getElementById('editform').setAttribute('action', '//tools.wmflabs.org/citations/text.php')");
	 cbButton.setAttribute("onkeydown", " document.getElementById('editform').setAttribute('action', '//tools.wmflabs.org/citations/text.php')");
	 diff.setAttribute("onmousedown", originalAction);
	 diff.setAttribute("onkeydown", originalAction);
	 document.getElementById('wpPreview').setAttribute("onmousedown", originalAction);
	 document.getElementById('wpPreview').setAttribute("onkeydown", originalAction);
	 document.getElementById('wpSave').setAttribute("onmousedown", originalAction);
	 document.getElementById('wpSave').setAttribute("onkeydown", originalAction);
	 diff.parentNode.insertBefore(cbButton, diff.nextSibling);
	}

}
$( addCitationBot );

if ( $.inArray( mw.config.get( 'skin' ), ['myskin', 'simple', 'monobook', 'modern', 'chick', 'vector'] ) !== -1 ) {
  $( addCBToToolbox );
}